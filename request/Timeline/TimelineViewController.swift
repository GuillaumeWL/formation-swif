//
//  TimelineViewController.swift
//  request
//
//  Created by Guillaume Lemaire on 02/10/2018.
//  Copyright © 2018 Guillaume Lemaire. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TimelineViewController: UIViewController, UITableViewDataSource {
    
    let urlApi = "http://api.yoannbraie.fr/timeline"
    @IBOutlet weak var tableView: UITableView!
    let cellIdentifier = "timelineDetails"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        apiCompenteces()
        tableView.register(UINib(nibName: "TimelineTableCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tlData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TimelineTableCell
        let timelineContent = tlData[indexPath.row]
        if let contentData = timelineContent["contenu"] {
            cell.timelineTitle?.text = contentData["titre"] as? String
            if let description = contentData["description"] as? String {
                if description != "" {
//                    print(description)
                    cell.timelineContent?.text = description
                    print(cell.timelineContent)
                }
            }
//            print(contentData)
        }
        return cell
    }
    var tlData = [[String:AnyObject]]()
    
    func apiCompenteces() {
        Alamofire.request(urlApi).responseJSON { response in
            if let data = response.result.value {
                let json = JSON(data)
                if let timelineData = json.arrayObject {
//                    self.tableData = competencesList as! [[String:AnyObject]]
//                    print(timelineData.count)
                    self.tlData = timelineData as! [[String : AnyObject]]
//                    print("tlData: \(self.tlData)")
//                    for data in timelineData {
//
////                        print("tlData: \(self.tlData)")
//                    }
                    self.tableView.reloadData()
                } else {
                    print("nothin to display : \(json)")
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
