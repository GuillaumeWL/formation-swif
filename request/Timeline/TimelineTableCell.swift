//
//  TimelineTableCell.swift
//  request
//
//  Created by Guillaume Lemaire on 02/10/2018.
//  Copyright © 2018 Guillaume Lemaire. All rights reserved.
//

import UIKit

class TimelineTableCell: UITableViewCell {

    @IBOutlet weak var timelineTitle: UILabel!
    @IBOutlet weak var timelineContent: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
