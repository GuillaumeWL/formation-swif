//
//  SecondViewController.swift
//  request
//
//  Created by Guillaume Lemaire on 27/09/2018.
//  Copyright © 2018 Guillaume Lemaire. All rights reserved.
//

import UIKit
import Alamofire

class SecondViewController: UIViewController {
    
    var count: Int = 0
    
    @IBOutlet var counter: UILabel!
    @IBOutlet var resetBtn: UIButton!
    @IBOutlet var changeViewBtn: UIButton!
    
    @IBAction func incrementCounter(_ sender: Any) {
        count += 1
        updateCounter()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        resetBtn.addTarget(self, action: #selector(fnResetTitle(button:)), for: .primaryActionTriggered)
        changeViewBtn.addTarget(self, action: #selector(fnChangeView(button:)), for: .primaryActionTriggered)
    }
    
    func updateCounter () {
        counter.text = "Counter : \(count)"
    }
    
    @objc func fnResetTitle(button:UIButton) {
        count = 0
        updateCounter()
    }
    
    @objc func fnChangeView(button:UIButton) {
        let thirdViewController = self.storyboard?.instantiateViewController(withIdentifier: "thirdstoryboard") as! ThirdViewController
        self.navigationController?.pushViewController(thirdViewController, animated: true)
    }
    
}
