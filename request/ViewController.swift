//
//  ViewController.swift
//  request
//
//  Created by Guillaume Lemaire on 26/09/2018.
//  Copyright © 2018 Guillaume Lemaire. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet var changeViewBtn: UIButton!
    @IBAction func changeView(_ sender: UIButton) {
        let vc = CompetencesViewController(nibName: "CompetencesViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func goToTimeline(_ sender: UIButton) {
        let vc = TimelineViewController(nibName: "TimelineViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    let urlApi = "http://api.yoannbraie.fr"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        apiMessage()
        changeViewBtn.addTarget(self, action: #selector(fnChangeView(button:)), for: .primaryActionTriggered)
    }
    
    func apiMessage() {
        Alamofire.request(urlApi).responseJSON { response in
            if let json = response.result.value as? [String: Any] {
                if let message = json["message"] as? String {
//                    print(message)
                    self.mainTitle.text = message
                }
            }
        }
    }
    
    @objc func fnChangeView(button:UIButton) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "secondstoryboard") as! SecondViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }

}
