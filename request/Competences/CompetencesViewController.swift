//
//  CompetencesViewController.swift
//  request
//
//  Created by Guillaume Lemaire on 28/09/2018.
//  Copyright © 2018 Guillaume Lemaire. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CompetencesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    let cellIdentifier = "competenceTableCell"
    let urlApi = "http://api.yoannbraie.fr/competences"
    var competencesList: [[String:AnyObject]] = [] // Mes données que je vais passer au controller suivant
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "CompetencesTableCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        apiCompenteces()
    }
    
    private var tableData = [[String:AnyObject]]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CompetencesTableCell
        let content = tableData[indexPath.row]
//        print("content :\(content)")
        cell.textLabel?.text = content["title"] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { // Quand je clique sur ma cellule
        let competenceDetailsViewController = CompetenceDetailsViewController() // Je déclare mon controller suivant, ici car sinon il rafraîchit pas les données
        let content = tableData[indexPath.row]
        competencesList = content["competences"] as! Array // Je récupère la liste de compétences en tableau
        competenceDetailsViewController.myDetails = competencesList // J'envoie mes données au controller suivant
        self.navigationController?.pushViewController(competenceDetailsViewController, animated: true) // et je redirige vers ce dernier
    }
    
    func apiCompenteces() {
        Alamofire.request(urlApi).responseJSON { response in
            if let data = response.result.value {
                let json = JSON(data)
                if let competencesList = json.arrayObject {
                    self.tableData = competencesList as! [[String:AnyObject]]
                } else {
                    print("nothin to display : \(json)")
                }
                self.tableView.reloadData()
            }
        }
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
