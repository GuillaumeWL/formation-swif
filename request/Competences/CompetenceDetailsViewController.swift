//
//  CompetenceDetailsViewController.swift
//  request
//
//  Created by Guillaume Lemaire on 28/09/2018.
//  Copyright © 2018 Guillaume Lemaire. All rights reserved.
//

import UIKit

class CompetenceDetailsViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    let cellIdentifier = "competenceDetailsCell"
    
    var myDetails: [[String:AnyObject]] = [] // Je déclare mon tableau de données

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        print("These are my details : \(myDetails)")
        tableView.register(UINib(nibName: "CompetencesDetailsViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CompetencesDetailsViewCell
//        print("details list : \(myDetails)")
        let details = myDetails[indexPath.row]
//        print("details : \(details)")
        cell.textLabel?.text = details["title"] as? String // J'affiche mes données envoyées dans myDetails par mon controller précédent
//        cell.textLabel?.text = "test"
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
