//
//  ThirdViewController.swift
//  request
//
//  Created by Guillaume Lemaire on 27/09/2018.
//  Copyright © 2018 Guillaume Lemaire. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ThirdViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let urlApi = "http://api.yoannbraie.fr/competences"
    @IBOutlet var tableView: UITableView!
    var rowsNb: Int = 0
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")!
        let content = tableData[indexPath.row]
        cell.textLabel?.text = content["title"] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiTimeline()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private var tableData = [[String:AnyObject]]()
    
    func apiTimeline() {
        Alamofire.request(urlApi).responseJSON { response in
            if let data = response.result.value {
                let json = JSON(data)
                if let competencesList = json.arrayObject {
                    self.tableData = competencesList as! [[String:AnyObject]]
                } else {
                    print("nothin to display : \(json)")
                }
                self.tableView.reloadData()
            }
        }
    }

    
    
}
